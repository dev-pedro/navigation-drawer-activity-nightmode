package activities;

import android.content.Context;
import android.content.SharedPreferences;

public class NightMode {
    private SharedPreferences sharedPreferencesNightMode;
    private String TagNightMode = "NIGHT_MODE";

    public NightMode(Context context) {
        sharedPreferencesNightMode = context.getSharedPreferences(TagNightMode, Context.MODE_PRIVATE);

    }

    public void setNightMode(Boolean nightMode) {
        SharedPreferences.Editor editor = sharedPreferencesNightMode.edit();
        editor.putBoolean(TagNightMode, nightMode).apply();
    }

    public boolean loadNightMode() {
        return sharedPreferencesNightMode.getBoolean(TagNightMode, false);
    }
}
